package org.lboot.md.service;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.io.file.FileNameUtil;
import lombok.SneakyThrows;
import org.lboot.md.constant.MarkdownConst;
import org.lboot.md.domain.MarkdownFile;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * @author kindear
 * 文件服务
 */
public interface MarkdownFileService {


    /**
     * 文件上传
     * @param file
     * @return
     */
    MarkdownFile uploadFile(MultipartFile file);



    /**
     * 获取文件信息
     * @param fileKey
     * @return
     */
    MarkdownFile getFile(String fileKey);




    /**
     * 获取文件流
     * @param fileKey
     * @return
     */
    InputStream getFileStream(String fileKey);







    /**
     * 更新文件信息
     * @param fileKey
     * @param stream
     * @return
     */
    boolean updateFile(String fileKey, InputStream stream);




//    /**
//     * 获取文件类型
//     *
//     * @param fileExt
//     * @return
//     */
//    default String getContentType(String fileExt) {
//        // 文件的后缀名
//        if ("bmp".equalsIgnoreCase(fileExt)) {
//            return "image/bmp";
//        }
//        if ("gif".equalsIgnoreCase(fileExt)) {
//            return "image/gif";
//        }
//        if ("jpeg".equalsIgnoreCase(fileExt) || "jpg".equalsIgnoreCase(fileExt) || ".png".equalsIgnoreCase(fileExt)) {
//            return "image/jpeg";
//        }
//        if ("png".equalsIgnoreCase(fileExt)) {
//            return "image/png";
//        }
//        if ("html".equalsIgnoreCase(fileExt)) {
//            return "text/html";
//        }
//        if ("txt".equalsIgnoreCase(fileExt)) {
//            return "text/plain";
//        }
//        if ("vsd".equalsIgnoreCase(fileExt)) {
//            return "application/vnd.visio";
//        }
//        if ("ppt".equalsIgnoreCase(fileExt) || "pptx".equalsIgnoreCase(fileExt)) {
//            return "application/vnd.ms-powerpoint";
//        }
//        if ("doc".equalsIgnoreCase(fileExt) || "docx".equalsIgnoreCase(fileExt)) {
//            return "application/msword";
//        }
//        if ("xml".equalsIgnoreCase(fileExt)) {
//            return "text/xml";
//        }
//        return "";
//    }
//
//
//    default ResponseEntity<byte[]> downloadMethod(InputStream in, HttpServletRequest request, String fileName){
//
//        HttpHeaders heads = new HttpHeaders();
//        heads.add(HttpHeaders.CONTENT_TYPE, "application/octet-stream; charset=utf-8");
//        try {
//            if (request.getHeader("User-Agent").toLowerCase().indexOf("firefox") > 0) {
//                // firefox浏览器
//                fileName = new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1);
//            } else if (request.getHeader("User-Agent").toUpperCase().indexOf("MSIE") > 0) {
//                // IE浏览器
//                fileName = URLEncoder.encode(fileName, "UTF-8");
//            } else if (request.getHeader("User-Agent").toUpperCase().indexOf("EDGE") > 0) {
//                // WIN10浏览器
//                fileName = URLEncoder.encode(fileName, "UTF-8");
//            } else if (request.getHeader("User-Agent").toUpperCase().indexOf("CHROME") > 0) {
//                // 谷歌
//                fileName = new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1);
//            } else {
//                //万能乱码问题解决
//                fileName = new String(fileName.getBytes(StandardCharsets.UTF_8), StandardCharsets.ISO_8859_1);
//            }
//        } catch (UnsupportedEncodingException e) {
//            // log.error("", e);
//        }
//        heads.add(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + fileName);
//        try {
//            // 输入流转换为字节流
//            byte[] buffer = FileCopyUtils.copyToByteArray(in);
//            //file.delete();
//            return new ResponseEntity<>(buffer, heads, HttpStatus.OK);
//        } catch (Exception e) {
//            // log.error("", e);
//        }
//        return null;
//    }

}
