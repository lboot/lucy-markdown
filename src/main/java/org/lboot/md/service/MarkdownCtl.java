package org.lboot.md.service;

import cn.hutool.core.io.file.FileNameUtil;
import cn.hutool.core.lang.Validator;
import cn.hutool.core.util.URLUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import org.apache.commons.lang3.StringUtils;
import org.lboot.md.constant.MarkdownConst;
import org.lboot.md.domain.MarkdownFile;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * @author kindear
 * markdown 服务定义
 */
public interface MarkdownCtl {
    /**
     * 构建远程文件信息
     * @param remoteUrl
     * @return
     */
    default MarkdownFile buildRemoteFile(String remoteUrl){
        HttpResponse response = HttpRequest.get(remoteUrl).execute();
        // log.info(response.headers().toString());
        String fileNameHeader = response.header(Header.CONTENT_DISPOSITION);
        // log.info(fileNameHeader);
        String contentType = response.header(Header.CONTENT_TYPE);
        // log.info(contentType);
        String fileName = remoteUrl;
        if (contentType.equals("application/octet-stream")){
            fileName = StringUtils.substringAfterLast(fileNameHeader,"=");
            fileName = URLUtil.decode(fileName);
            if (Validator.isNotEmpty(fileName)){
                fileName = fileName.replace("UTF-8''", "");
            }
        }else if (contentType.startsWith("image")){
            // 图片
            fileName = StringUtils.substringAfterLast(remoteUrl,"/");
        }else {
            fileName = StringUtils.substringAfterLast(fileNameHeader,"=");
            fileName = URLUtil.decode(fileName);
            if (Validator.isNotEmpty(fileName)){
                fileName = fileName.replace("UTF-8''", "");
            }
        }
        // 进行MD5 编码
        String fileKey = SecureUtil.md5(response.bodyStream());
        // 构建 Document
        MarkdownFile document = new MarkdownFile();
        document.setKey(fileKey);
        document.setTitle(fileName);
        String fileType = FileNameUtil.extName(fileName);
        document.setFileType(fileType);
        document.setUrl(remoteUrl);
        //log.info(document.toString());
        document.setContent(response.body());
        return document;
    }

    /**
     * 预览远程文件
     * @param remoteUrl
     * @return
     */
    default ModelAndView previewRemote(String remoteUrl){
        MarkdownFile file = buildRemoteFile(remoteUrl);
        return preview(file);
    }

    /**
     * 自定义内容预览
     * @param file
     * @return
     */
    default ModelAndView preview(MarkdownFile file){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("mdpreview");
        modelAndView.addObject("file",file);
        return modelAndView;
    }

    /**
     * 根据文件唯一识别 key 预览 需要拓展第三方实现
     * @param fileKey
     * @return
     */
    ModelAndView preview(String fileKey);


    /**
     * 根据文件唯一识别 key 编辑
     * @param fileKey
     * @return
     */
    ModelAndView edit(String fileKey);


    /**
     * 编辑回调
     * @param map
     * @return
     */
    public Object editCallback(Map<String, Object> map);


    /**
     * 文件上传
     * @param file
     * @return 文件可访问地址
     */
     String fileUpload(MultipartFile file);


}
