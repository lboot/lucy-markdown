package org.lboot.md.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.lboot.md.domain.MarkdownFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;

@Slf4j
@RequiredArgsConstructor
public class DefaultMarkdownFileService implements MarkdownFileService{
    @Override
    public MarkdownFile uploadFile(MultipartFile file) {
        return null;
    }

    @Override
    public MarkdownFile getFile(String fileKey) {
        return null;
    }

    @Override
    public InputStream getFileStream(String fileKey) {
        return null;
    }

    @Override
    public boolean updateFile(String fileKey, InputStream stream) {
        return false;
    }
}
