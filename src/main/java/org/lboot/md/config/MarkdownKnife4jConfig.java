package org.lboot.md.config;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author kindear
 */
@Configuration
@EnableSwagger2
@AllArgsConstructor
public class MarkdownKnife4jConfig {

    @Bean(value = "markdownApi")
    public Docket markdownApi() {
        String groupName="MD服务API";
        ApiInfo apiInfo = new ApiInfoBuilder()
                .title("MD服务API")
                .description("Markdown预览编辑服务")
                .termsOfServiceUrl("http://localhost:8080")
                .version("1.0.0")
                .build();
        return new Docket(DocumentationType.SWAGGER_2)
                .host("http://localhost:8080/")
                .apiInfo(apiInfo)
                .groupName(groupName)
                .select()
                .apis(RequestHandlerSelectors.basePackage("org.lboot.md"))
                .paths(PathSelectors.any())
                .build();
    }
}
