package org.lboot.md.config;


import org.lboot.md.service.DefaultMarkdownFileService;
import org.lboot.md.service.MarkdownFileService;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

@SpringBootConfiguration
public class MarkdownServiceConfig {
    // 存储服务 默认使用 minio

    @Bean
    @ConditionalOnMissingBean(value = MarkdownFileService.class)
    public MarkdownFileService markdownFileService(){
        return new DefaultMarkdownFileService();
    }
}
