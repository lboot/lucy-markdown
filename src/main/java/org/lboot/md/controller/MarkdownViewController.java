package org.lboot.md.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.lboot.md.service.MarkdownCtl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author kindear
 */
@Slf4j
@Controller
@RequestMapping("vditor")
@Api(tags = "MD预览编辑")
@AllArgsConstructor
public class MarkdownViewController {
    MarkdownCtl markdownCtl;

    @RequestMapping("preview/remote")
    @ApiOperation(value = "网络MD文档预览",notes = "支持.md")
    public ModelAndView previewRemote(@RequestParam("url") String url){
        return markdownCtl.previewRemote(url);
    }

    @RequestMapping("preview/{key}")
    @ApiOperation(value = "MD文档预览",notes = "支持.md")
    public ModelAndView previewMarkdown(@PathVariable("key") String key){
        log.info("文件key:{}", key);
        return markdownCtl.preview(key);
    }

    @RequestMapping("edit/{key}")
    @ApiOperation(value = "MD文档编辑",notes = "支持.md")
    public ModelAndView editMarkdown(@PathVariable("key") String key){
        return markdownCtl.edit(key);
    }
}
