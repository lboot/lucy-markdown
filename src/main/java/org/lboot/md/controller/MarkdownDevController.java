package org.lboot.md.controller;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.io.file.FileNameUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.lboot.md.domain.MarkdownFile;
import org.lboot.md.service.MarkdownFileService;
import org.lboot.s3.client.S3Client;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@RequestMapping("vditor/test")
@Api(tags = "lucy-markdown 测试服务")
@AllArgsConstructor
@ConditionalOnProperty(prefix = "lucy.markdown",name = "mode",havingValue = "dev")
public class MarkdownDevController {

    MarkdownFileService fileService;

    @Data
    @ApiModel(value = "文件上传参数")
    public static class TestFileUploadParams{
        @ApiModelProperty("文件")
        private MultipartFile file;
    }

    S3Client s3Client;
    @SneakyThrows
    @PostMapping("import")
    @ApiOperation(value = "MD文件导入")
    public Object markdownImport(TestFileUploadParams params){
        return fileService.uploadFile(params.getFile());
    }
}
