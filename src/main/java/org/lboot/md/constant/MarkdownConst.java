package org.lboot.md.constant;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class MarkdownConst {
    /**
     * 获取临时文件存储目录
     * @return 目录路径
     */
    public static String tempFilePath(){
        String baseDir = System.getProperty("user.dir");
        return baseDir+ File.separator + "temp" + File.separator;
    }
    // 图像类型
    public static final Set<String> IMG_FILE = new HashSet<String>(){
        {
            add("bmp");
            add("jpg");
            add("png");
            add("tif");
            add("gif");
            add("jpeg");
        }
    };
    // 文档类型
    public static final Set<String> DOC_FILE = new HashSet<String>(){
        {
            add("doc");
            add("docx");
            add("ppt");
            add("pptx");
            add("xls");
            add("xlsx");
            add("pdf");
            add("txt");
        }
    };

    // 音乐类型
    public static final Set<String> MUSIC_FILE = new HashSet<String>(){
        {
            add("wav");
            add("aif");
            add("au");
            add("mp3");
            add("ram");
            add("wma");
            add("mmf");
            add("amr");
            add("aac");
            add("flac");
        }
    };
    // 视频类型
    public static final Set<String> VIDEO_FILE = new HashSet<String>(){
        {
            add("avi");
            add("mp4");
            add("mpg");
            add("mov");
            add("swf");
            add("flv");
        }
    };
}
