package org.lboot.md.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author kindear
 * markdown 文件实体类信息
 */
@Data
@ApiModel(value = "markdown文件信息")
public class MarkdownFile {

    /** 【必需】文件唯一标识 */
    @ApiModelProperty(value = "文档 key", example="xYz123")
    private String key;

    /** 【必需】文档名称 */
    @ApiModelProperty(value = "文档标题", example="test.doc")
    private String title;


    /** 【必需】 文档后缀 **/
    @ApiModelProperty(value = "文档类型", example="doc")
    private String fileType;

    /** 【必需】 文档资源地质 **/
    @ApiModelProperty(value = "文档获取url", example="http://10.4.89.60:8080/api/file/xYz123")
    private String url;

    @ApiModelProperty(value = "文档内容")
    private String content;
}
