package org.lboot.md.domain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author kindear
 * markdown 文档预览/编辑配置
 */
@Slf4j
@Data
@ApiModel(value = "文档预览/编辑配置")
public class MarkdownConfig {

    @ApiModelProperty(value = "模式",example = "edit/preview")
    String mode;

    @ApiModelProperty("是否启用侧边栏")
    boolean sidebar;

}
