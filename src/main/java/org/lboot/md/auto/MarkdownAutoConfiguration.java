package org.lboot.md.auto;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;

/**
 * @author kindear
 * mardown
 */
@Slf4j
@AutoConfiguration
@ComponentScan(basePackages = {
        "org.lboot.md"
})
public class MarkdownAutoConfiguration implements EnvironmentAware {
    @Override
    public void setEnvironment(Environment environment) {
       // 在此处理
    }
}
